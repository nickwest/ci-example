defmodule CiExample.Repo do
  use Ecto.Repo,
    otp_app: :ci_example,
    adapter: Ecto.Adapters.Postgres
end
