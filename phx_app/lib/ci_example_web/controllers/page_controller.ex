defmodule CiExampleWeb.PageController do
  use CiExampleWeb, :controller

  def index(conn, _params) do
    render(conn, "index.html")
  end
end
