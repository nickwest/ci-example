# GitLab Runner CI Example

Instructions on how to install/configure a `gitlab-runner` for **GitLab CI**, with an example
GitLab CI config file (`.gitlab-ci.yml`) and an example Elixir/Phoenix app.

The runner for this repo is hosted on a free-tier **AWS EC2 Ubuntu 18.04** spot instance.

[Alternatives for installing on Kubernetes/etc](https://docs.gitlab.com/runner/#install-gitlab-runner)

## Install GitLab CI with Docker Executor

### CentOS

**Install GitLab Runner**
```shell
sudo yum update
# get rpm gitlab-runner install script
curl -L https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.rpm.sh | sudo bash
# install gitlab-runner
sudo yum install gitlab-runner
```

**Install Docker CE**
```shell
# Add yum-utils + support packages
sudo yum install -y yum-utils \
  device-mapper-persistent-data \
  lvm2

# Add stable docker-ce repo
sudo yum-config-manager \
  --add-repo \
  https://download.docker.com/linux/centos/docker-ce.repo

# Install docker-ce
sudo yum install docker-ce docker-ce-cli containerd.io

# Start docker
sudo systemctl start docker

# OPTIONAL test docker
sudo docker run hello-world
```

### Ubuntu

**Install GitLab Runner**
```shell
sudo apt-get update

# get debian gitlab-runner install script
curl -L https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.deb.sh | sudo bash

# install gitlab-runner
sudo apt-get install gitlab-runner
```

**Install Docker CE**
```shell
# Packages for communication over HTTPS
sudo apt-get install     apt-transport-https     ca-certificates     curl     software-properties-common

# Add docker Ubuntu GPG key
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo add-apt-repository    "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
$(lsb_release -cs) \
stable"

#Install docker-ce
sudo apt-get install docker-ce

# Test to see if docker works
sudo docker run hello-world
```

### Register & Start Runner

1. Run the runner registration command:
```shell
sudo gitlab-runner register
```

2. Get CI config URL and token from your GitLab repository's `Settings > CI / CD > Runners > Specific Runners` info GUI
and enter them when prompted

<img src="https://i.imgur.com/XAuOjkN.png"  width="600">

3. Enter a description/tags/etc (optional)

4. When prompted, choose your **executor** (`docker`, `kubernetes`, `shell`, etc)

If you choose `docker`, you must choose a default image for when none are defined in `.gitlab-ci.yml`, e.g.: `alpine:latest`

5. Start runner:
```shell
sudo gitlab-runner start
```

### Install HTTPS Communication & Docker Packages

**CentOS**

```shell
# Add yum-utils + support packages
sudo yum install -y yum-utils \
  device-mapper-persistent-data \
  lvm2

# Add stable docker-ce repo
sudo yum-config-manager \
  --add-repo \
  https://download.docker.com/linux/centos/docker-ce.repo

# Install docker-ce
sudo yum install docker-ce docker-ce-cli containerd.io

# Start docker
sudo systemctl start docker

# OPTIONAL test docker
sudo docker run hello-world
```

**Ubuntu**

```shell
# Packages for communication over HTTPS
sudo apt-get install     apt-transport-https     ca-certificates     curl     software-properties-common

# Add docker Ubuntu GPG key
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo add-apt-repository    "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
$(lsb_release -cs) \
stable"

#Install docker-ce
sudo apt-get install docker-ce

# Test to see if docker works
sudo docker run hello-world
```

### If using a shell executor...

#### Enable sudo on Runner User

```
sudo usermod -a -G sudo gitlab-runner
sudo visudo
```

**Warning:** Do not do this for a runner that can be executed by untrusted users!
Give the gitlab-runner user permission to run the commands it needs to run via `sudo`:
```
gitlab-runner ALL=(ALL) NOPASSWD: ALL
```

More securely, give the gitlab-runner user permission to only run the commands needed by the script:

```
gitlab-runner ALL=(ALL) NOPASSWD: /usr/bin/some_cmd
```

Note re: **SSH Users**:

You can also configure the gitlab-runner to connect to a remote host via SSH, with a remote
user that has the level of permissions required by your run script.

### Install Erlang & Elixir

For CI of the enclosed Elixir app using a `shell` executor,
run the following commands on the server to ensure user access to `mix` commands:

```
wget https://packages.erlang-solutions.com/erlang-solutions_1.0_all.deb && sudo dpkg -i erlang-solutions_1.0_all.deb
sudo apt-get install esl-erlang
sudo apt install elixir
```

## CI Config

Our GitLab Runner gets its instructions from the `.gitlab-ci.yml` config file in our repo.

Note that the config file within this repo is specifically for a runner using Docker as its executor,
and that referencing container images for services + running commands without `sudo` will cause shell executors to fail,
as they require access to locally installed services (such as databases) as well as the ability to manage local packages (in most cases).

For more info on GitLab CI config, [check out the docs](https://docs.gitlab.com/ee/ci/yaml/)
